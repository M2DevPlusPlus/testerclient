import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TestResponse } from 'src/app/Classes/TestResponse';

@Component({
  selector: 'app-test-response',
  templateUrl: './test-response.component.html',
  styleUrls: ['./test-response.component.css']
})
export class TestResponseComponent implements OnInit {

  constructor() { }
  @Input() response: TestResponse;
  @Input() httpType: string;
  @Output() closed:EventEmitter<any>=new EventEmitter()
  n = { aa: 'aaa' };
  ngOnInit(): void {
    debugger;
    console.log(this.response);
    console.log(this.httpType);

    // this.response.;
  }
  close(){
this.closed.emit()
  }
}
