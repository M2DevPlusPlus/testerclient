import { Component, OnInit, Input } from '@angular/core';
import { Path } from 'src/app/Classes/Path';
import { SwaggerService } from 'src/app/Services/swagger.service';
import { ApiService } from 'src/app/Services/api.service';
import { Test } from 'src/app/Classes/Test';
import { System } from 'src/app/Classes/System';
import { ActivatedRoute, Params } from '@angular/router';
import { Field } from 'src/app/Classes/Field';
import { BodySample } from 'src/app/Classes/BodySample';
import { TestResponse } from 'src/app/Classes/TestResponse';
// import { BodySchema } from 'src/app/Classes/BodySchema';

@Component({
  selector: 'app-tester',
  templateUrl: './tester.component.html',
  styleUrls: ['./tester.component.css']
})
export class TesterComponent implements OnInit {

  path: { name: string, sysInd: number, CurrentPath: Path };
  test: Test;
  system: System;
  components: JSON;
  testResponse: TestResponse;
  errResponse: any;
  hasResponse = false;
  loadTesting = false;
  bodySample: any;
  bodySchema: any;
  requestBody: string;
  isSample: boolean;
  isSchema: boolean;
  showResponse = false;
  SampleShow: boolean;
  currentEnum: string[] = [];
  CurrentFieldId: any;
  TempEnum: string[];

  constructor(private testerService: ApiService, private route: ActivatedRoute, private swaggerService: SwaggerService) { }
  testMode: boolean;
  ngOnInit() {
    this.route.params.subscribe((paramsFromUrl: Params) => {
      if (!this.testerService.ApisList)
        this.testerService.goHome()
      this.SampleShow = true

      this.isSchema = true;
      this.isSample = false;
      this.testMode = false;
      this.testResponse = new TestResponse('', '');
      this.bodySample = '';
      const name = paramsFromUrl.name;
      const sysId = paramsFromUrl.sysId;
      const pathId = paramsFromUrl.pathId;
      this.system = this.testerService.ApisList[sysId];
      const Current = this.system.pathTbl[paramsFromUrl.pathId];
      this.path = { sysInd: paramsFromUrl.sysId, CurrentPath: Current, name: paramsFromUrl.name };
      this.components = this.testerService.ComponentsPerApi[this.system.systemId];
      const req: Field = this.path.CurrentPath.fieldTbl.find(a => a.fieldIn === 'body' || a.fieldName === "requestBody");
      if (req !== undefined) {
        this.requestBody = BodySample.GetSampleFromField(req, this.components);
        this.bodySample = JSON.parse(this.requestBody);
        this.bodySchema = this.components;
        console.log(this.bodySample);
        this.isSample = true;
      }
      this.test = new Test('', {}, this.requestBody, '');
    });

  }
  parse(prop) {
    return JSON.parse(prop);
  }
  fillSample() {
    if (this.test.RequestBody === '') {
      this.test.RequestBody = this.bodySample;
    }
  }

  Collapse() {
    document.getElementById("Samples").classList.add("w3-hide")
    this.SampleShow = false
  }
  unCollapse() {
    document.getElementById("Samples").classList.remove("w3-hide")
    this.SampleShow = true

  }
  isEnum(field: Field) {
    if (field.fieldType.startsWith("enum")) {
      debugger
      if (!(this.CurrentFieldId === field.fieldId)) {
        this.CurrentFieldId === field.fieldId;
        this.currentEnum=[]
        this.TempEnum = field.fieldType.replace('enum:[', '').replace(']', '').split(',')
        this.TempEnum.forEach(e => {
          var d1 = e.replace('\n', '')
          var d2 = d1.trim()
          var d3 = d2.replace('"', '').replace('"', '')
         this.currentEnum.push(d3);
        })
          
      }
      return true;
    }
    return false
  }

  isRequiredFull() {
    debugger
    for (const field of this.path.CurrentPath.fieldTbl) {
      if (field.require &&
        !((this.test.Properties[field.fieldName] != undefined && this.test.Properties[field.fieldName] != null && this.test.Properties[field.fieldName] != "") ||
          ((field.fieldName == 'requestBody' || field.fieldIn == "body") && this.test.RequestBody != undefined)))
        return false
    }
    return true
  }
  testPath() {
    // debugger;
    this.testResponse = new TestResponse("", "")
    this.loadTesting = true;
    this.test.Url = this.system.url + this.path.CurrentPath.url;
    this.test.TypePath = this.path.CurrentPath.httpType;
    this.swaggerService.test(this.test).subscribe(
      d => {
        debugger
        this.testResponse = d;
        this.loadTesting = false
        console.log(d.requwstURL);
        console.log(this.testResponse.requwstURL);
        this.showResponse = true;
      }
    );
    // this.testerService.TestPath(this.test).subscribe(
    //   data => {
    //     debugger;
    //     console.log(data);
    //     this.testResponse = data;
    //     this.i = true;
    //   },
    //   err => this.errResponse = err,
    //   () => { this.hasResponse = true; this.loadTesting = false; }

    // );

  }
}
