import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/Services/api.service';
import { System } from 'src/app/Classes/System';
import { Path } from 'src/app/Classes/Path';
import { Router } from '@angular/router';
class pathDisplay {
  controller: string
  pathes: { ind: number; name: string;shortName:string, path: Path }[]
}
@Component({
  selector: 'app-apis-tester',
  templateUrl: './brix-apis.component.html',
  styleUrls: ['./brix-apis.component.css']
})



export class BrixApisComponent implements OnInit {
  isLoad: boolean;
  load: number
  disp: number;
  actPath: number;
  PathDisp: pathDisplay[] = []
  CurrentTest: { "name": string, "sysInd": number; "CurrentPath": Path; };
  test: number = -1;
  isSidebar: boolean;
  constructor(public apiService: ApiService, private location: Router) { }
  crdLogo = "../../assets/imgs/crb_logo_docs.svg"
  loadIcon = "../../assets/imgs/loader1.svg"
  ngOnInit() {


    this.CurrentTest = { name: "", sysInd: -1, CurrentPath: new Path() }
    this.load = -1
    this.apiService.getAllApis().subscribe(
      (data: System[]) => {
        debugger
        console.log(data);
        this.apiService.ApisList = data;
        this.isLoad = true;
        // let sys: System = JSON.parse('{"systemId":0,"url":"http://localhost:7777","description":"","swaggerUrl":null,"pathTbl":[{"pathId":0,"systemId":0,"url":"/api/SignupCard/TestOauth","pathReturn":null,"description":"","httpType":"get","pathName":null,"fieldTbl":[]},{"pathId":0,"systemId":0,"url":"/api/Card","pathReturn":null,"description":"","httpType":"put","pathName":null,"fieldTbl":[{"fieldId":0,"pathId":0,"fieldName":"requestBody","fieldType":"PTPE.WebApi.Contracts.ChangeCardStatusRequest","description":null,"require":false}]},{"pathId":0,"systemId":0,"url":"/api/Card","pathReturn":null,"description":"","httpType":"post","pathName":null,"fieldTbl":[{"fieldId":0,"pathId":0,"fieldName":"requestBody","fieldType":"PTPE.WebApi.Contracts.SignupCardRequest","description":null,"require":false}]},{"pathId":0,"systemId":0,"url":"/api/Card/{cardToken}","pathReturn":null,"description":"","httpType":"get","pathName":null,"fieldTbl":[{"fieldId":0,"pathId":0,"fieldName":"cardToken","fieldType":"string","description":null,"require":false}]},{"pathId":0,"systemId":0,"url":"/api/card","pathReturn":null,"description":"","httpType":"get","pathName":null,"fieldTbl":[{"fieldId":0,"pathId":0,"fieldName":"take","fieldType":"integer","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"pageNumber","fieldType":"integer","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"dateAddedFrom","fieldType":"date-time","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"dateAddedTo","fieldType":"date-time","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"firstName","fieldType":"string","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"lastName","fieldType":"string","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"isActive","fieldType":"boolean","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"cardCompany","fieldType":"enum","description":null,"require":false}]},{"pathId":0,"systemId":0,"url":"/api/Enum/CardCompanies","pathReturn":null,"description":"","httpType":"get","pathName":null,"fieldTbl":[]},{"pathId":0,"systemId":0,"url":"/api/Enum/TransactionStatuses","pathReturn":null,"description":"","httpType":"get","pathName":null,"fieldTbl":[]},{"pathId":0,"systemId":0,"url":"/api/Health/WebApiCheck","pathReturn":null,"description":"","httpType":"get","pathName":null,"fieldTbl":[]},{"pathId":0,"systemId":0,"url":"/api/PushToCard/TestOauth","pathReturn":null,"description":"","httpType":"get","pathName":null,"fieldTbl":[]},{"pathId":0,"systemId":0,"url":"/api/transaction","pathReturn":null,"description":"","httpType":"get","pathName":null,"fieldTbl":[{"fieldId":0,"pathId":0,"fieldName":"cardToken","fieldType":"string","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"take","fieldType":"integer","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"pageNumber","fieldType":"integer","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"statusEnum","fieldType":"enum","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"fromDate","fieldType":"date-time","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"toDate","fieldType":"date-time","description":null,"require":false}]},{"pathId":0,"systemId":0,"url":"/api/transaction","pathReturn":null,"description":"","httpType":"post","pathName":null,"fieldTbl":[{"fieldId":0,"pathId":0,"fieldName":"requestBody","fieldType":"Shared.Contracts.PushToCardRequest","description":null,"require":false}]},{"pathId":0,"systemId":0,"url":"/api/transaction/{cardToken}/{transactionId}","pathReturn":null,"description":"","httpType":"get","pathName":null,"fieldTbl":[{"fieldId":0,"pathId":0,"fieldName":"cardToken","fieldType":"string","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"transactionId","fieldType":"string","description":null,"require":false}]},{"pathId":0,"systemId":0,"url":"/api/transaction/{transactionId}","pathReturn":null,"description":"","httpType":"get","pathName":null,"fieldTbl":[{"fieldId":0,"pathId":0,"fieldName":"transactionId","fieldType":"string","description":null,"require":false}]},{"pathId":0,"systemId":0,"url":"/api/WebhookRegistrations","pathReturn":null,"description":"","httpType":"get","pathName":null,"fieldTbl":[{"fieldId":0,"pathId":0,"fieldName":"page","fieldType":"integer","description":null,"require":false},{"fieldId":0,"pathId":0,"fieldName":"pageSize","fieldType":"integer","description":null,"require":false}]},{"pathId":0,"systemId":0,"url":"/api/WebhookRegistrations","pathReturn":null,"description":"","httpType":"post","pathName":null,"fieldTbl":[{"fieldId":0,"pathId":0,"fieldName":"requestBody","fieldType":"PTPE.WebApi.Contracts.WebhookRegistrationRequest","description":null,"require":false}]},{"pathId":0,"systemId":0,"url":"/api/WebhookRegistrations","pathReturn":null,"description":"","httpType":"delete","pathName":null,"fieldTbl":[{"fieldId":0,"pathId":0,"fieldName":"requestBody","fieldType":"PTPE.WebApi.Contracts.RemoveWebhookRegistrationRequest","description":null,"require":false}]}]}') as System;

        debugger
        // this.apiService.ApisList.find(s => s.description == "DebitPush").pathTbl = sys.pathTbl;

      }
    )
  }
  showSideBar() {
    document.getElementById("sideBar").classList.replace("w3-hide-small", "w3-show"),
      document.getElementById("body").classList.add("w3-hide")

    document.getElementById("sideBar").style.width = "100%"
    document.getElementById("sideBar").style.maxWidth = "100%"
    this.isSidebar = true
  }
  hideSideBar() {
    document.getElementById("sideBar").classList.replace("w3-show", "w3-hide-small")
    // document.getElementById("sideBar").style.maxWidth="initial"
    document.getElementById("body").classList.remove("w3-hide")
    document.getElementById("sideBar").style.maxWidth = "290px"

    this.isSidebar = false
  }


  LoadApi(ind: number) {
    debugger
    if (this.disp == ind) {
      this.disp = -1;
      return
    }
    this.disp=-1
    if (this.load == ind)
      return
    this.load = ind
    if (this.apiService.ApisList[ind].pathTbl == undefined) {
      this.apiService.GetSingleApi(this.apiService.ApisList[ind]).subscribe(
        (arr: any[]) => {

          this.apiService.ApisList[ind] = arr[0];
          this.apiService.ComponentsPerApi[this.apiService.ApisList[ind].systemId] = arr[1] as JSON
          this.actPath = 0;
          this.fillPath(this.apiService.ApisList[ind].pathTbl);
          this.disp = ind;
          this.load = -1
        },
        err => {
          this.load = -1
          alert("Failed to load api,please try later")

        }
      )

    }


    else {
      this.fillPath(this.apiService.ApisList[ind].pathTbl)
      this.disp = ind;
      this.load = -1
      if( this.apiService.ComponentsPerApi[this.apiService.ApisList[ind].systemId]==null)
        this.apiService.GetComponents(this.apiService.ApisList[ind]).subscribe(
          data=>this.apiService.ComponentsPerApi[this.apiService.ApisList[ind].systemId]=data as JSON
        )
    }


  }

  fillPath(pathList: Path[]) {
    debugger
    this.PathDisp = []
    for (let index = 0; index < pathList.length; index++) {
      const p = pathList[index];
      var url = p.url.replace('/api', '')
      var arr = url.split('/');
      var key =p.component;
      // var value = arr.splice(1, 1);
      var i = this.PathDisp.findIndex(p => p.controller == key)
      if (i == -1) {
        i = this.PathDisp.push({ controller:key, pathes: [] }) - 1;
      }
      // arr.splice(0,2)
      var value = arr.join('/');
      arr.splice(0,2)
     var value2= arr.join('/')
      this.PathDisp[i].pathes.push({ ind: index, name: value,shortName:value2, path: p })
    };
    debugger
  }


  testPath(si, pi, path, name) {
    debugger
    this.test = -1
    this.CurrentTest = { name: name, sysInd: si, CurrentPath: path }
    // this.apiService.CurrentTest=this.CurrentTest;
    this.test = pi;
    this.location.navigate(['/BrixTester/TestPath', name, si, pi])
  }

}
