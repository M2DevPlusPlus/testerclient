import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrixApisComponent } from './brix-apis.component';

describe('BrixApisComponent', () => {
  let component: BrixApisComponent;
  let fixture: ComponentFixture<BrixApisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrixApisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrixApisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
