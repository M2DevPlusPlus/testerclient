import { Field } from './Field'

export class Path{
    pathId:string
    sytemId:string
    url:string
    pathReturn:string
    description:string
    httpType:string
    component:string
    pathName:string
    fieldTbl:Array<Field>
}