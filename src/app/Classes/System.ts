import { Path } from './Path'

export class System {
    systemId: string
    url: string
    description: string
    swaggerUrl:string
    pathTbl: Array<Path>
}