export class Field{
    fieldId:string
    pathId:string
    systemId:string
    fieldName:string
    fieldType:string
    description:string
    require:boolean=true
    fieldIn:string
}