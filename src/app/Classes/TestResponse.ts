
export class TestResponse {
    constructor(
        public requwstURL: string,
        public responseBody: string,
        public responseCode?: string,
        public responseHeaders?: { key: string, value: Array<string> },

    ) { }
}
