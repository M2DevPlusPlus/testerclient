import { Field } from './Field';

export class BodySample {
    static components: JSON
    public static GetSampleFromField(req: Field, comp: JSON) {
        debugger
        this.components = comp;
        var type = req.fieldType;

        if (type.startsWith("array")) {
            // var t = type.split('<' || '>')[1];
            var t = type.substring(type.lastIndexOf("<") + 1, type.lastIndexOf(">"));
            return '[' + this.GetObjSampleByName(t) + ']'
        }
        else if (type.startsWith("dictionary")) {
            var t = type.substring(type.lastIndexOf("<") + 1, type.lastIndexOf(">"));
            return this.getDictionaryExemple("string", t)
        }
        else if (type.startsWith("enum")) {
            var e = type.replace('enum', '')
            var options = e.split(',')
            return options[0].toString()
        }
        else {
            return this.GetObjSampleByName(type)
        }
    }
    static GetObjSampleByName(type: string) {
        debugger
        if (type == "string")
            return '"string"'
        else if (type == "integer" || type == 'number')
            return '9999'

        else if (type == "date" || type == 'date-time')
            return '"' + new Date().toJSON() + '"'
        else if (type == 'boolean')
            return '"true"'
        else if ((this.components.hasOwnProperty("schemas") && this.components["schemas"].hasOwnProperty(type))) {
            return this.getObjectByTypeObj(this.components["schemas"][type])
        }
        else if (this.components.hasOwnProperty(type)) {
            return this.getObjectByTypeObj(this.components[type])
        }
        else
            return '{}'

    }
    static getObjectByTypeObj(typeObj: JSON) {
        debugger
        var exemple = "{"
        var props = typeObj["properties"]
        for (const key in props) {
            var value = ""
            if (props[key].hasOwnProperty("example")) {
                value = '"' + props[key]["example"] + '"'
            }
            else if (props[key].hasOwnProperty("type")) {

                if (props[key]["type"] == "array") {//object is array
                    if (props[key].hasOwnProperty("items")) {//array of objects
                        if (props[key]["items"].hasOwnProperty("$ref")) {
                            const s: string[] = props[key]["items"]["$ref"].split('/')
                            value = "[" + this.GetObjSampleByName(s[s.length - 1]) + ']'
                        }
                        else if (props[key]["items"].hasOwnProperty("type")) {//array os simple type
                            if (props[key]["items"]["type"] == "string" && props[key]["items"].hasOwnProperty("format") && props[key]["items"]["format"] == "date-time")
                                value = "[" + this.GetObjSampleByName("date") + ']'
                            else
                                value = "[" + this.GetObjSampleByName(props[key]["items"]["type"]) + ']'
                        }
                    }
                }
                else if (props[key]["type"] == "object") {//object is dictionary
                    if (props[key].hasOwnProperty("additionalProperties")) {
                        if (props[key]["additionalProperties"].hasOwnProperty("$ref")) {
                            var val = props[key]["additionalProperties"]["$ref"].split('/')
                            var valType = val[val.length - 1]
                            value = this.getDictionaryExemple("string", valType)
                        }
                        else if (props[key]["additionalProperties"].hasOwnProperty("type")) {
                            var val = props[key]["additionalProperties"]["type"]
                            value = this.getDictionaryExemple("string", val)
                        }
                    }
                    else value = '{}'

                }
                else {//object is single simple object 
                    //object type is date
                    if (props[key]["type"] == "string" && props[key].hasOwnProperty("format") && props[key]["format"] == "date-time")
                        value = this.GetObjSampleByName("date")
                    else//all other types
                        value = this.GetObjSampleByName(props[key]["type"])
                }
            }
            else if (props[key].hasOwnProperty("allOf")) {//object is single type object
                if (props[key]["allOf"][0].hasOwnProperty("$ref")) {
                    const s: string[] = (props[key]["allOf"])[0]["$ref"].split('/')
                    value = this.GetObjSampleByName(s[s.length - 1])
                }
            }
            else if (props[key].hasOwnProperty("$ref")) {
                const s: string[] = props[key]["$ref"].split('/')
                value = this.GetObjSampleByName(s[s.length - 1])

            }
            else {//default
                value = '{}'
            }

            exemple += '"' + key.toString() + '"' + ":" + value + ','
        }
        if (exemple[exemple.length - 1] == ',')
            exemple = exemple.slice(0, -1)
        exemple += '}'
        return exemple;
    }


    static getDictionaryExemple(keyType: string, valueType: string) {
        var exe = "{"
        var self = this
        var key = keyType == "integer" ? "" : "prop"
        for (let index = 1; index < 3; index++) {
            exe += '"' + key + index + '"' + ":" + this.GetObjSampleByName(valueType) + ','
        }
        if (exe[exe.length - 1] == ',')
            exe = exe.slice(0, -1)
        exe += '}'
        return exe;
    }
}