
export interface IHash {
    [name: string] : string;
} 


export class Test {

    constructor(
        public Url: string,
        public Properties: { [id: string]: string; },
        public RequestBody: string,
        public TypePath: string
    ) { }

}