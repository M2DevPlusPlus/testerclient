import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { System } from '../Classes/System';
import { Api } from '../Classes/Api';
import { Test } from '../Classes/Test';
import { TestResponse } from '../Classes/TestResponse';

@Injectable({
  providedIn: 'root'
})
export class SwaggerService {
  api: Api;
  fieldEdit: any;
  fieldsToEdit: { [id: string]: string; } = {};
  sysToEdit: { [id: string]: string; } = {};
  pathDescToEdit: { [id: string]: string; } = {};
  pathReturnToEdit: { [id: string]: string; } = {};
  constructor(private http: HttpClient) {
    this.api = new Api('', '');
  }
  system: System;
  components: any;
  GetApi() {
    debugger;
    console.log('before get api', new Date());

    this.http.post('/api/Swagger/ParallelGetApi', this.api).subscribe(
      (data: System) => {
        debugger;
        this.system = data;
        console.log(data);
      }
    );
  }
  GetComponentsOfApi() {
    this.http.post('/api/Swagger/GetComponentsOfApi', this.api).subscribe(
      (data: JSON) => {
        debugger;
        this.components = data;
        console.log(data);
      }
    );
  }
  initEditArrays() {
    this.sysToEdit = {};
    this.pathDescToEdit = {};
    this.pathReturnToEdit = {};
    this.fieldsToEdit = {};
  }
  SaveAllChanges() {
    debugger;
    // tslint:disable-next-line:max-line-length
    return this.http.post('/api/Swagger/ChangeDesc', { sys: this.sysToEdit, pathDesc: this.pathDescToEdit, pathReturn: this.pathReturnToEdit, field: this.fieldsToEdit });
  }
  test(t: Test) {
    return this.http.post<TestResponse>('/api/Tester/TestPath', t);
  }
}
