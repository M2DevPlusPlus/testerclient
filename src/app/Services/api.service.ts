import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { System } from '../Classes/System';
import { Api } from '../Classes/Api';
import { Test } from '../Classes/Test';
import { Path } from '../Classes/Path';
import { TestResponse } from '../Classes/TestResponse';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  BASIC_URL = '';
  ApisList: System[];
  ComponentsPerApi: { [apiId: number]: JSON } = {};
  CurrentTest: { name: string; sysInd: number; CurrentPath: Path; };

  constructor(private http: HttpClient,private location:Router) { }
  goHome() {
    this.location.navigateByUrl('/')
  }
  getAllApis() {
    debugger;
    return this.http.get('api/Tester/GetAllSystems');

  }
  GetComponents(sys: System) {
    const api: Api = new Api(sys.url, sys.swaggerUrl);
    return this.http.post('/api/Swagger/GetComponentsOfApi', api);
  }
  GetSingleApi(sys: System) {
    const api: Api = new Api(sys.url, sys.swaggerUrl);
    debugger;
    return this.http.post('/api/Swagger/GetApi', api);
  }

  TestPath(test: Test) {
    debugger;
    return this.http.post<TestResponse>('/api/Tester/test', test);
  }
}
