import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms'
import { AppComponent } from './app.component';
import { Routing } from 'src/Routing';
import { BrixApisComponent } from './Components/brix-apis/brix-apis.component';
import { HttpClientModule } from '@angular/common/http';
import { TesterComponent } from './Components/tester/tester.component';
import { MainMenuComponent } from './Components/main-menu/main-menu.component';
import { TestResponseComponent } from './Components/test-response/test-response.component';
import { Route } from '@angular/compiler/src/core';

@NgModule({
  declarations: [
    AppComponent,BrixApisComponent, TesterComponent, MainMenuComponent, TestResponseComponent
  ],
  imports: [
    BrowserModule,RouterModule,FormsModule,HttpClientModule,
    RouterModule.forRoot(Routing, {enableTracing: true})  

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
