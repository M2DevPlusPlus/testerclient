import { Route } from "@angular/router";
import { BrixApisComponent } from './app/Components/brix-apis/brix-apis.component';
import { TesterComponent } from './app/Components/tester/tester.component';
import { MainMenuComponent } from './app/Components/main-menu/main-menu.component';

export const Routing: Route[] = [
    { path: '', redirectTo: 'BrixTester/Home', pathMatch: 'full' },

    {
        path: 'BrixTester',
        component: BrixApisComponent,
        children: [
            {
                path: 'TestPath/:name/:sysId/:pathId',
                component: TesterComponent
            },
            {
                path: 'Home',
                component: MainMenuComponent
            }
        ]


    },
   // {
    //     path: 'Tester',
    //     component: BrixTesterDesignComponent,
    //     childern: [
    //         {
    //             path: 'Home',
    //             component: TesterMainMenuComponent
    //         },
    //         {
    //             path: 'TestPath',
    //             component:TesterComponent
    //         }
    //     ]
    // },

    // {
    //     path: 'ApiDisplay/:ind',
    //     component: ApiDisplayComponent
    // }

]